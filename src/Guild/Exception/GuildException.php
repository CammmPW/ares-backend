<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Guild\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class GuildException
 *
 * @package Ares\Guild\Exception
 */
class GuildException extends BaseException
{

}
