<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Photo\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class PhotoException
 *
 * @package Ares\Photo\Exception
 */
class PhotoException extends BaseException
{

}