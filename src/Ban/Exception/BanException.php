<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Ban\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class BanException
 *
 * @package Ares\Ban\Exception
 */
class BanException extends BaseException
{
}
