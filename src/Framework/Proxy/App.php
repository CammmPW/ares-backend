<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Framework\Proxy;

use Statical\BaseProxy;


/**
 * Class App
 *
 * @package Ares\Framework\Proxy
 */
class App extends BaseProxy
{
}