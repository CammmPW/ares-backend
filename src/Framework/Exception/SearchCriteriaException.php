<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Framework\Exception;


/**
 * Class SearchCriteriaException
 *
 * @package Ares\Framework\Exception
 */
class SearchCriteriaException extends BaseException
{
}
