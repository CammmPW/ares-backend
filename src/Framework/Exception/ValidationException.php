<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Framework\Exception;

/**
 * Class ValidationException
 *
 * @package Ares\Framework\Exception
 */
class ValidationException extends BaseException
{
}
