<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Framework\Exception;

/**
 * Class AuthenticationException
 *
 * @package Ares\Framework\Exception
 */
class AuthenticationException extends BaseException
{
}