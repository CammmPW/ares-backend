<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Framework\Entity;

use JsonSerializable;

/**
 * Class Entity
 *
 * @package Ares\Framework\Entity
 */
abstract class Entity implements \Serializable, JsonSerializable
{
}
