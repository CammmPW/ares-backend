<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Guestbook\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class GuestbookException
 *
 * @package Ares\Guestbook\Exception
 */
class GuestbookException extends BaseException
{
}