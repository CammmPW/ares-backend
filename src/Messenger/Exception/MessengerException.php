<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Messenger\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class MessengerException
 *
 * @package Ares\Messenger\Exception
 */
class MessengerException extends BaseException
{

}
