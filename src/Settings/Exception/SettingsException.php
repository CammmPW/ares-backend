<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Settings\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class SettingsException
 *
 * @package Ares\Settings\Exception
 */
class SettingsException extends BaseException
{
}
