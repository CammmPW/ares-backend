<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Article\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class ArticleException
 *
 * @package Ares\Article\Exception
 */
class ArticleException extends BaseException
{
}
