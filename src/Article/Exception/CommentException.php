<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Article\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class CommentException
 *
 * @package Ares\Article\Exception
 */
class CommentException extends BaseException
{
}