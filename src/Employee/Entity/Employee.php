<?php declare(strict_types=1);
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Employee\Entity;

use Ares\Framework\Entity\Entity;
use Ares\User\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * Class Employee
 *
 * @package Ares\Employee\Entity
 *
 * @ORM\Table(name="permissions")
 * @ORM\Entity(repositoryClass="Ares\Employee\Repository\EmployeeRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class Employee extends Entity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $rank_name;

    /**
     * @ORM\Column(type="string")
     */
    private string $badge;

    /**
     * @OneToMany(targetEntity="\Ares\User\Entity\User", mappedBy="employeeData", fetch="EAGER")
     */
    private ?Collection $employees;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Employee
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getRankName(): ?string
    {
        return $this->rank_name;
    }

    /**
     * @param string $rank
     *
     * @return string
     */
    public function setRankName(?string $rank): self
    {
        $this->rank_name = $rank;

        return $this;
    }

    /**
     * @return string
     */
    public function getBadge(): string
    {
        return $this->badge;
    }

    /**
     * @param string $badge
     *
     * @return string
     */
    public function setBadge(string $badge): self
    {
        $this->badge = $badge;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getEmployees(): ?Collection
    {
        return $this->employees;
    }

    /**
     * @param Collection|null $employees
     *
     * @return Employee
     */
    public function setEmployees(?Collection $employees): self
    {
        $this->employees = $employees;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'rank_name' => $this->getRankName(),
            'badge' => $this->getBadge(),
            'employees' => $this->getEmployees()->toArray()
        ];
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize(get_object_vars($this));
    }

    /**
     * @param string $data
     */
    public function unserialize($data): void
    {
        $values = unserialize($data);

        foreach ($values as $key => $value) {
            $this->$key = $value;
        }
    }
}
