<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Employee\Controller;

use Ares\Framework\Controller\BaseController;
use Ares\Framework\Model\Adapter\DoctrineSearchCriteria;
use Ares\Employee\Exception\EmployeeException;
use Ares\Employee\Repository\EmployeeRepository;
use Ares\User\Exception\UserException;
use Jhg\DoctrinePagination\Collection\PaginatedArrayCollection;
use Phpfastcache\Exceptions\PhpfastcacheSimpleCacheException;
use Psr\Cache\InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class EmployeeController
 *
 * @package Ares\Employee\Controller
 */
class EmployeeController extends BaseController
{
    /**
     * @var EmployeeRepository
     */
    private EmployeeRepository $employeeRepository;

    /**
     * @var DoctrineSearchCriteria
     */
    private DoctrineSearchCriteria $searchCriteria;

    /**
     * EmployeeController constructor.
     *
     * @param   EmployeeRepository     $employeeRepository
     * @param   DoctrineSearchCriteria  $searchCriteria
     */
    public function __construct(
        EmployeeRepository $employeeRepository,
        DoctrineSearchCriteria $searchCriteria
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->searchCriteria = $searchCriteria;
    }

    /**
     * @param   Request   $request
     * @param   Response  $response
     *
     * @param             $args
     *
     * @return Response
     * @throws EmployeeException
     * @throws UserException
     * @throws PhpfastcacheSimpleCacheException
     * @throws InvalidArgumentException
     */
    public function list(Request $request, Response $response, $args): Response
    {
        /** @var int $page */
        $page = $args['page'];

        /** @var int $resultPerPage */
        $resultPerPage = $args['rpp'];

        $this->searchCriteria->setPage((int)$page)
            ->setLimit((int)$resultPerPage)
            ->addOrder('id', 'DESC');

        $friends = $this->employeeRepository->paginate($this->searchCriteria);

        if ($friends->isEmpty()) {
            throw new EmployeeException(__('No staff members found'), 404);
        }

        return $this->respond(
            $response,
            response()->setData([
                'employees' => $friends->toArray()
            ])
        );
    }
}
