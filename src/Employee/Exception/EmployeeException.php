<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Employee\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class EmployeeException
 *
 * @package Ares\Employee\Exception
 */
class EmployeeException extends BaseException
{

}
