<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Room\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class RoomException
 *
 * @package Ares\Room\Exception
 */
class RoomException extends BaseException
{
}
