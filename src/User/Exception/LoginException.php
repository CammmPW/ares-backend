<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\User\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class LoginException
 *
 * @package Ares\User\Exception
 */
class LoginException extends BaseException
{
}
