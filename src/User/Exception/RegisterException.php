<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\User\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class RegisterException
 *
 * @package Ares\User\Exception
 */
class RegisterException extends BaseException
{
}
