<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

namespace Ares\Vote\Exception;

use Ares\Framework\Exception\BaseException;

/**
 * Class VoteException
 *
 * @package Ares\Vote\Exception
 */
class VoteException extends BaseException
{
}