<?php
/**
 * Ares (https://ares.to)
 *
 * @license https://gitlab.com/arescms/ares-backend/LICENSE (MIT License)
 */

declare(strict_types=1);

(require __DIR__ . '/../app/bootstrap.php')->run();
